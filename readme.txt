How to clear Cache
-------------------

COMMANDS to RUN

1. Clear Cache:
php ./vendor/bundles/Sensio/Bundle/DistributionBundle/Resources/bin/build_bootstrap.php


2. Building Tables:
 a) First make sure you have database named "sportsdirectory"
 b) Execute: php app/console doctrine:schema:update --force
