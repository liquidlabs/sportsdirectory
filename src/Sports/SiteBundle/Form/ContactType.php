<?php

namespace Sports\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;


class ContactType extends AbstractType {
		
	
	public function buildForm(FormBuilder $builder, array $opts) {
		
		$builder->add('name');
		$builder->add('email', 'email');
		$builder->add('body', 'textarea');
	}
	
	public function getName() {
		return 'Contact';
	}
}
