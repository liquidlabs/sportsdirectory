<?php

namespace Sports\SiteBundle\Helper;

/**
 * A Utlity class which is accessed all throughout
 * the Application (TheSportsDirectory.ca)
 */

class ToolBox {
	
	/* ideally, it should be private, but in symfony, i need to make it public */
	public function __construct() {}	
	private $seoStopWords = array('a','and','the','an','it','is','with','can','of','why','not', 'under');
	
		
	
	/**
	 * Returns a Lat and Lng from an Address using Google Geocoder API. It does not 
	 * require any Google API Key
	 *
	 * @param $opt 	An array containing
	 *			    'address' => The Address to be parsed
	 *			    'sensor' => 'true' or 'false' as [string]
	 *
	 * @return 		An array containing
	 *				'status'  => Boolean which is true on success, false on failure
	 *				'message' => 'Success' on success, otherwise an error message
	 *			    'lat'	  => The Lat of the address
	 *				'lon'	  => The Lng of the address
	 *
	 */
	function getLatLon($opts) {
		
		/* grab the XML */
		$url = 'http://maps.googleapis.com/maps/api/geocode/xml?' 
			. 'address=' . $opts['address'] . '&sensor=' . $opts['sensor'];
		
		$dom = new \DomDocument();
		
		// create curl resource 
        $ch = curl_init(); 

        // set url 
        curl_setopt($ch, CURLOPT_URL, $url); 

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        // $output contains the output string 
        $output = curl_exec($ch); 

        // close curl resource to free up system resources 
        curl_close($ch);  		
		
		$dom->loadXML($output);
		
		/* A response containing the result */
		$response = array();
		
		$xpath = new \DomXPath($dom);
		$statusCode = $xpath->query("//status");
	
		/* ensure a valid StatusCode was returned before comparing */
		if ($statusCode != false && $statusCode->length > 0 
			&& $statusCode->item(0)->nodeValue == "OK") {
		
			$latDom = $xpath->query("//location/lat");
			$lonDom = $xpath->query("//location/lng");
			
			/* if there's a lat, then there must be lng :) */
			if ($latDom->length > 0) {
				
				$response = array (
					'status' 	=> true,
					'message' 	=> 'Success',
					'lat' 		=> $latDom->item(0)->nodeValue,
					'lon' 		=> $lonDom->item(0)->nodeValue
				);
	
				return $response;
			}	
			
		}
	
		$response = array (
			'status' => false,
			'message' => "Oh snap! Error in Geocoding. Please check Address"
		);
		return $response;
	}


	/**
	 * Generates an SEO Friendly Link
	 * Code originally taken from (but modified to suit needs):
	 * http://davidwalsh.name/generate-search-engine-friendly-urls-php-function
	 * 
	 * @param
	 * 
	 **/
	function generateSeoLink($input, $replace = '-', $words_array = false)
	{
		if (!$words_array) {
			$words_array = $this->seoStopWords;
		}			
		
		$replace = '-';
				
		//make it lowercase, remove punctuation, remove multiple/leading/ending spaces
		$return = trim(ereg_replace(' +',' ',preg_replace('/[^a-zA-Z0-9\s]/','',strtolower($input))));
	
		//remove words, if not helpful to seo
		//i like my defaults list in remove_words(), so I wont pass that array
//		if($remove_words) {
		 $return = $this->removeWords($return, $replace, $words_array); 
//		}
	
		//convert the spaces to whatever the user wants
		//usually a dash or underscore..
		//...then return the value.
		return str_replace(' ', $replace, $return);
	}
	
	/* takes an input, scrubs unnecessary words */
	function removeWords($input, $replace, $words_array = array(), $unique_words = true)
	{
		//separate all words based on spaces
		$input_array = explode(' ',$input);
	
		//create the return array
		$return = array();
	
		//loops through words, remove bad words, keep good ones
		foreach($input_array as $word)
		{
			//if it's a word we should add...
			if(!in_array($word,$words_array) && ($unique_words ? !in_array($word,$return) : true))
			{
				$return[] = $word;
			}
		}
	
		//return good words separated by dashes
		return implode($replace, $return);
	} 
	
}
