<?php

/**
 * 
 */

namespace Sports\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


//use Doctrine\Common\Collections\ArrayCollection;


/**
 * This maps to the Spot of the database
 * Each Spot has have a Sport associated with it
 * So, its a Many to One relationship with Sport
 * i.e. Many Spot can have One Sport with it.
 */ 

/**
 * @ORM\Entity
 * @ORM\Table(name="user_spots") 
 */ 
class UserSpot {
	
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 */		
	protected $id;
	
	/**
	 * @ORM\Column(type="integer", name="spot_id")
	 */
	protected $spotId;
	
	/**
	 * @ORM\Column(type="string", length=100)
	 */
	protected $title;
	
	
	/**
	 * @ORM\Column(type="string", length=200)
	 */
	protected $addressTitle;
	
	
	/**
	 * @ORM\Column(type="string", length=200)
	 */
	protected $path; 
	
	/**
	 * @ORM\Column(type="string", length=200)
	 */
	protected $address;
	
	
	/**
	 * @ORM\Column(type="string", length=100)
	 */
	protected $city;
	
	
	/**
	 * @ORM\Column(type="string", length=10)
	 */	
	protected $postalCode;
	
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $state;
	

	/**
	 * @ORM\Column(type="string", length=200)
	 */	
	protected $url;
	
	
	/**
	 * @ORM\Column(type="text")
	 */	
	protected $description;
	
	
	/**
	 * @ORM\Column(type="string", length=20)
	 */	
	protected $phone;
	
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $sport;
	

	/**
	 * @ORM\Column(type="integer")
	 */		
	protected $likes;
	
	/**
	 * @ORM\Column(name="lat", type="decimal", precision="10", scale="7")
	 */
	protected $lat;	

	/**
	 * @ORM\Column(name="lon", type="decimal", precision="10", scale="7")
	 */
	protected $lon;	

	
	/**
	 * @ORM\Column(name="active", type="integer")
	 */
	protected $active;
	
	/**
	 * @ORM\Column(name="created_at", type="datetime")
	 */
	protected $createdAt; 
	
	
	/**
	 * Default Constructor, setting the datetime
	 */
	public function __construct() {
		parent::__construct();
		$this->createdAt  = new \DateTime("now");
	}
	
	
	/**
	 * 
	 */
	public function setSpot(\Sports\SiteBundle\Entity\Spot $spot) {
		$this->title = $spot->title;
		$this->address = $spot->address;
		$this->city = $spot->city;
		$this->phone = $spot->phone;
		$this->active = 0;
	}
	
	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set address
     *
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * Get postalCode
     *
     * @return string 
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set url
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set phone
     *
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set state
     *
     * @param Sports\SiteBundle\Entity\State $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * Get state
     *
     * @return Sports\SiteBundle\Entity\State 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set sport
     *
     * @param Sports\SiteBundle\Entity\Sport $sport
     */
    public function setSport($sport)
    {
        $this->sport = $sport;
    }

    /**
     * Get sport
     *
     * @return Sports\SiteBundle\Entity\Sport 
     */
    public function getSport()
    {
        return $this->sport;
    }

    /**
     * Set addressTitle
     *
     * @param string $addressTitle
     */
    public function setAddressTitle($addressTitle)
    {
        $this->addressTitle = $addressTitle;
    }

    /**
     * Get addressTitle
     *
     * @return string 
     */
    public function getAddressTitle()
    {
        return $this->addressTitle;
    }

    /**
     * Set path
     *
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set likes
     *
     * @param integer $likes
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;
    }


    /**
     * Get likes
     *
     * @return integer 
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * Set lat
     *
     * @param decimal $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * Get lat
     *
     * @return decimal 
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param decimal $lon
     */
    public function setLon($lon)
    {
        $this->lon = $lon;
    }

    /**
     * Get lon
     *
     * @return decimal 
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set id
     *
     * @param integer $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set spotId
     *
     * @param integer $spotId
     */
    public function setSpotId($spotId)
    {
        $this->spotId = $spotId;
    }

    /**
     * Get spotId
     *
     * @return integer 
     */
    public function getSpotId()
    {
        return $this->spotId;
    }

    /**
     * Set active
     *
     * @param integer $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}