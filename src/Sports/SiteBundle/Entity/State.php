<?php

namespace Sports\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="states")
 */
class State {
	
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	 protected $id;
	 
	 
	 /**
	  * @ORM\Column(type="string", length=200)
	  */
	  protected $name;
	  
	  
	  /**
	   * @ORM\Column(type="string", length=200)
	   */
	  protected $code;
	  
	  
	  /**
	   * @ORM\Column(type="integer")
	   */
	  protected $active;
	  
	  
	  /**
	   * @ORM\OneToMany(targetEntity="Spot", mappedBy="sports")
	   */
	  protected $spots;
	 
	 
	  /**
	   * Constructor 
	   */
	  public function __construct()  {
	 	 $this->spots = new ArrayCollection();
	  }
	  
	  
	  public function setCode($c) {
	  	$this->code = $c;
	  }
	  
	  public function getCode() {
	  	return $this->code;
	  }
	  
	
	  public function setName($n) {
		$this->name = $n;
	  }	  

	  public function getName() {
		return $this->name;
	  }	  
	  
	  
	  public function setActive($status) {
	 	$this->active = $status;
	  }
	 
	  public function getActive() {
	 	return $this->active;
	  }
	 



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add spots
     *
     * @param Sports\SiteBundle\Entity\Spot $spots
     */
    public function addSpot(\Sports\SiteBundle\Entity\Spot $spots)
    {
        $this->spots[] = $spots;
    }

    /**
     * Get spots
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getSpots()
    {
        return $this->spots;
    }
}